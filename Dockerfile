FROM node:lts

# Specify where our app will live in the container
WORKDIR /app

# Copy the React App to the container
COPY . /app/

# Prepare the container for building React
RUN npm ci
# We want the production version
RUN npm run build