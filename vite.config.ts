import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react";
import fs from "fs";

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd(), "");

  return {
    server: {
      host: env.VITE_HOST,
      https: {
        key: fs.readFileSync(".cert/key.pem"),
        cert: fs.readFileSync(".cert/cert.pem"),
      },
    },
    plugins: [react()],
  };
});
