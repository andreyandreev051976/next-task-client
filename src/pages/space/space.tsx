import { Heading } from "@chakra-ui/react";
import { BaseLayout } from "../../shared/layout";
import { MySpaces } from "../../widgets/space/ui";

export const SpacePage = () => (
  <BaseLayout.Container>
    <Heading mb="1rem">My spaces</Heading>
    <MySpaces />
  </BaseLayout.Container>
);
