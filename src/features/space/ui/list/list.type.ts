import { ISpaceDto } from "../../../../entities/space/model/space";

export interface ISpaceList {
  spaces: ISpaceDto[];
}
