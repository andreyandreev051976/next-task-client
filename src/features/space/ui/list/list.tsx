import { Grid, GridItem } from "@chakra-ui/react";
import { ISpaceList } from "./list.type";
import { SpaceCard } from "../../../../entities/space/ui";

export const SpaceList = ({ spaces }: ISpaceList) => {
  return (
    <Grid templateColumns="repeat(6, 1fr)" gap={6}>
      {spaces.map((space) => (
        <GridItem key={space.id} w="100%">
          <SpaceCard {...space} />
        </GridItem>
      ))}
    </Grid>
  );
};
