import { useFormContext } from "react-hook-form";
import { ICreateSpaceDto } from "../../../../../../entities/space/model/space";
import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
} from "@chakra-ui/react";

export const SpaceName = () => {
  const {
    register,
    formState: { errors },
  } = useFormContext<ICreateSpaceDto>();

  const isInvalid = Boolean(errors["name"]?.message);

  return (
    <FormControl mb=".4rem" isInvalid={isInvalid}>
      <FormLabel>Name</FormLabel>
      <Input {...register("name")} />
      {isInvalid && (
        <FormErrorMessage>{errors["name"]?.message}</FormErrorMessage>
      )}
    </FormControl>
  );
};
