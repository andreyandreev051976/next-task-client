import { Button } from "@chakra-ui/react";
import { ISubmit } from "./submit.type";
import { FORM_ID } from "../../../../model/create/create.constants";

export const Submit = (props: ISubmit) => {
  return <Button {...props} form={FORM_ID} type="submit" />;
};
