import { Controller, useFormContext } from "react-hook-form";
import { ICreateSpaceDto } from "../../../../../../entities/space/model/space";
import {
  Badge,
  FormControl,
  FormErrorMessage,
  FormLabel,
} from "@chakra-ui/react";
import { Select, SelectItem } from "../../../../../../shared/components/select";
import {
  IUserDto,
  allUsersStore,
  userStore,
} from "../../../../../../entities/user/model";
import { useEffect, useMemo } from "react";
import { UserCard } from "../../../../../../entities/user/ui";

export const SpaceUsers = () => {
  const {
    control,
    formState: { errors },
  } = useFormContext<ICreateSpaceDto>();

  const { actions, getters } = allUsersStore();

  useEffect(() => {
    actions.getAll();
  }, [actions]);

  const isInvalid = Boolean(errors["users"]?.message);

  const allUsers = getters.withoutCurrent(userStore);

  const usersById = useMemo(
    () =>
      allUsers.reduce((byId, user) => {
        byId[user.id] = user;
        return byId;
      }, {} as Record<IUserDto["id"], IUserDto>),
    [allUsers]
  );

  return (
    <FormControl isInvalid={isInvalid}>
      <FormLabel>Users</FormLabel>
      <Controller
        control={control}
        name="users"
        render={({ field }) => (
          <Select
            selectType="multiple"
            selected={field.value}
            onItemSelect={field.onChange}
            closeOnSelect={false}
            head={
              <Select.Head gap=".4rem">
                {({ selected }) => (
                  <>
                    {selected.map((id) => (
                      <Badge key={id as string}>
                        {usersById[id as number].email}
                      </Badge>
                    ))}
                  </>
                )}
              </Select.Head>
            }
          >
            <Select.List maxH="15rem">
              {allUsers.map((user) => (
                <SelectItem mb="0.2rem" key={user.id} selectValue={user.id}>
                  {({ isSelected }) => (
                    <UserCard {...user} isSelected={isSelected} />
                  )}
                </SelectItem>
              ))}
            </Select.List>
          </Select>
        )}
      />
      {isInvalid && (
        <FormErrorMessage>{errors["users"]?.message}</FormErrorMessage>
      )}
    </FormControl>
  );
};
