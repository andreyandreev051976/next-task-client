import { useFormContext } from "react-hook-form";
import { ICreateSpaceDto } from "../../../../../../entities/space/model/space";
import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Switch,
} from "@chakra-ui/react";

export const SpaceIsPrivate = () => {
  const {
    register,
    formState: { errors },
  } = useFormContext<ICreateSpaceDto>();

  const isInvalid = Boolean(errors["isPrivate"]?.message);

  return (
    <FormControl mb=".4rem" display="flex" alignItems="center" isInvalid={isInvalid}>
      <FormLabel htmlFor="private">Is Private</FormLabel>
      <Switch {...register("isPrivate")} id="private" />
      {isInvalid && (
        <FormErrorMessage>{errors["isPrivate"]?.message}</FormErrorMessage>
      )}
    </FormControl>
  );
};
