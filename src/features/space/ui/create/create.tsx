import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";
import { GlobalModalManager } from "../../../../shared/widgets";
import { CreateSpaceLogic } from "../../model";
import { SpaceName } from "./components/name";
import { SpaceIsPrivate } from "./components/isPrivate";
import { SpaceUsers } from "./components/users";
import { Submit } from "./components/submit";

export const CreateSpace = () => {
  const { createSpace } = GlobalModalManager.useModalManager();

  return (
    <Modal isOpen={createSpace.isOpen} onClose={createSpace.close}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Create Space</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <CreateSpaceLogic>
            <SpaceName />
            <SpaceIsPrivate />
            <SpaceUsers />
          </CreateSpaceLogic>
        </ModalBody>
        <ModalFooter>
          <Submit>Create</Submit>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
