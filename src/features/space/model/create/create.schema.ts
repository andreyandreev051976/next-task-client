import * as yup from "yup";

export const schema = yup
  .object({
    name: yup.string().required(),
    isPrivate: yup.bool().required(),
    users: yup.array(yup.number().required()).required()
  })
  .required();
