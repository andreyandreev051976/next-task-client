import { yupResolver } from "@hookform/resolvers/yup";
import { FormProvider, useForm } from "react-hook-form";
import { schema } from "./create.schema";
import { PropsWithChildren, useCallback } from "react";
import { Box, useToast } from "@chakra-ui/react";
import { ICreateSpaceDto } from "../../../../entities/space/model/space";
import { createSpace } from "../../../../entities/space/api";
import { FORM_ID } from "./create.constants";
import { GlobalModalManager } from "../../../../shared/widgets";
import { mySpaceStore } from "../../../../entities/space/model";

const useSubmit = () => {
  const { createSpace: createSpaceModal } =
    GlobalModalManager.useModalManager();
  const { actions } = mySpaceStore()
  const toast = useToast();
  const onSubmit = useCallback(
    async (form: ICreateSpaceDto) => {
      try {
        await createSpace({ data: form });
        await actions.getMy()
        toast({
          status: "success",
          title: "Created",
        });
        createSpaceModal.close();
      } catch (error) {
        toast({
          status: "error",
          title: "Error",
        });
      }
    },
    [toast, actions, createSpaceModal]
  );

  return { onSubmit };
};

export const CreateSpaceLogic = ({ children, ...props }: PropsWithChildren) => {
  const { onSubmit } = useSubmit();
  const form = useForm<ICreateSpaceDto>({
    resolver: yupResolver(schema),
    defaultValues: {
      isPrivate: false,
      users: [],
    },
  });

  return (
    <FormProvider {...form}>
      <Box
        as="form"
        id={FORM_ID}
        {...props}
        onSubmit={form.handleSubmit(onSubmit)}
      >
        {children}
      </Box>
    </FormProvider>
  );
};
