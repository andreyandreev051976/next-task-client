import { forwardRef, ListItem as ListItemBase } from '@chakra-ui/react';

import { IListItem } from './item.type';


export const ListItem = forwardRef<IListItem, 'li'>(({ isActive, ...props }, ref) => {
  return <ListItemBase role="listitem" tabIndex={0} {...props} ref={ref} />;
});
