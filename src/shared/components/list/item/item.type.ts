import { ListItemProps } from '@chakra-ui/react';

export interface IListItem extends ListItemProps {
  isActive?: boolean;
}
