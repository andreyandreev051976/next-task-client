import { defineStyle, forwardRef, List as ListBase, ListProps } from '@chakra-ui/react';

const baseStyles = defineStyle({
  p: '0.5rem',
});

export const List = forwardRef<ListProps, 'ul'>((props, ref) => <ListBase {...baseStyles} {...props} ref={ref} />);
