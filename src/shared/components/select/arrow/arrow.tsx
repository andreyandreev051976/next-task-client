import { defineStyle, forwardRef, IconProps, StyleProps, usePopoverContext } from '@chakra-ui/react';

import { ChevronDownIcon } from '@chakra-ui/icons';

const openStyles: [StyleProps, StyleProps] = [{}, { transform: 'rotate(180deg)' }];

const baseStyles = defineStyle({
  transformOrigin: 'center',
  transition: 'transform 200ms',
  cursor: 'pointer',
});

export const HeadArrow = forwardRef<IconProps, 'svg'>((props, ref) => {
  const { isOpen } = usePopoverContext();

  const popoverStyles = openStyles[+isOpen];
  return <ChevronDownIcon {...baseStyles} {...popoverStyles} {...props} ref={ref} />;
});
