import { MouseEventHandler } from 'react';
import { usePopoverContext } from '@chakra-ui/react';

import { ListItem } from '../../list';
import { useSelect } from '../select.hook';

import { ISelectItem } from './item.type';
import { WithSelect, withSelect } from '../../../logic/select';

export const SelectItem = withSelect<WithSelect<ISelectItem>>(({ isSelected, onClick, isActive, ...props }) => {
  const { closeOnSelect } = useSelect();
  const { onClose } = usePopoverContext();
  const handleClick: MouseEventHandler = (event) => {
    onClick?.(event);
    if (closeOnSelect) {
      onClose?.();
    }
  };
  return (
    <ListItem
      cursor="pointer"
      borderRadius={0}
      onClick={handleClick}
      {...props}
      isActive={isSelected || isActive}
    />
  );
});
