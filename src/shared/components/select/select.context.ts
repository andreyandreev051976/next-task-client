import { createContext } from 'react';

import { ISelectCtx } from './select.type';

export const SelectCtx = createContext({} as unknown as ISelectCtx);
