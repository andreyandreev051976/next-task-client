import { ComponentPropsWithRef, PropsWithChildren, ReactNode } from 'react';
import { Popover } from '@chakra-ui/react';

import { ISelect as ISelectLogic } from '../../logic/select';

export interface ISelectCtx {
  closeOnSelect?: boolean;
}

export type ISelect = PropsWithChildren<
  Omit<ComponentPropsWithRef<typeof Popover>, 'children'> &
    ISelectLogic &
    ISelectCtx & {
      head?: ReactNode;
    }
>;
