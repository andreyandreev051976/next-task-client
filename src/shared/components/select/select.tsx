import { useMemo } from 'react';
import { Popover, PopoverBody, PopoverTrigger } from '@chakra-ui/react';

import { Select as SelectLogic } from '../../logic/select';

import { List } from '../list';

import { SelectBody } from './container';
import { SelectHead } from './head';
import { SelectCtx } from './select.context';
import { ISelect } from './select.type';
import { Head } from './head/head';

const DefaultHead = <SelectHead />;

export const Select = ({
  selected: outerSelected,
  onItemSelect,
  selectType = 'single',
  head = DefaultHead,
  children,
  offset = [0, 0],
  flip = false,
  matchWidth = true,
  closeOnSelect = true,
  ...props
}: ISelect) => {
  const selectCtxValue = useMemo(() => ({ closeOnSelect }), [closeOnSelect]);
  return (
    <SelectCtx.Provider value={selectCtxValue}>
      <SelectLogic selected={outerSelected} onItemSelect={onItemSelect} selectType={selectType}>
        <Popover {...props} flip={flip} offset={offset} matchWidth={matchWidth}>
          <PopoverTrigger>{head}</PopoverTrigger>
          <SelectBody overflow="auto">
            <PopoverBody px="0" pt="0" pb="0.625rem">
              {children}
            </PopoverBody>
          </SelectBody>
        </Popover>
      </SelectLogic>
    </SelectCtx.Provider>
  );
};

Select.List = List;
Select.Head = Head;
