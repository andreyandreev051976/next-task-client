import { useMemo } from 'react';
import { forwardRef } from '@chakra-ui/react';

import { SelectConsumer } from '../../../logic/select';

import { SelectHeadArrow } from '../arrow';

import { HeadContainer } from './container';
import { ISelectHead } from './head.type';

export const DefaultHead: ISelectHead['children'] = (ctx) => {
  const { selected } = ctx;
  return selected.length === 0 ? 'select' : selected;
};

export const Head = forwardRef(({ children = DefaultHead, withArrow = true, ...props }: ISelectHead, ref) => {
  const content = useMemo(() => (typeof children === 'function' ? children : () => children), [children]);
  return (
    <HeadContainer w="100%" {...props} ref={ref}>
      <SelectConsumer>{content}</SelectConsumer>
      {withArrow && <SelectHeadArrow ml="auto" />}
    </HeadContainer>
  );
});
