import { ReactNode } from 'react';

import { ISelectCtx } from '../../../logic/select/select.context';

import { ISelectHeadContainer } from './container/container.type';

export type ISelectHead = Omit<ISelectHeadContainer, 'children'> & {
  children?: ((ctx: ISelectCtx) => ReactNode) | ReactNode;
  withArrow?: boolean;
};