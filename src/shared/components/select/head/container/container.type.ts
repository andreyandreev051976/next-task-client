import { ComponentPropsWithoutRef } from 'react';
import { Button } from '@chakra-ui/react';

export type ISelectHeadContainer = ComponentPropsWithoutRef<typeof Button>;
