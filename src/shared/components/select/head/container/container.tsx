import { Box, chakra, forwardRef } from "@chakra-ui/react";

import { ISelectHeadContainer } from "./container.type";

const HeadContent = chakra(Box, {
  baseStyle: {
    py: "0.5rem",
    px: "1rem",
    border: '1px solid',
    borderColor: 'gray.200',
    borderRadius: '.5rem',
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
});

export const HeadContainer = forwardRef(
  ({ children, ...props }: ISelectHeadContainer, ref) => {
    return (
      <HeadContent {...props} variant="clear" ref={ref}>
        {children}
      </HeadContent>
    );
  }
);
