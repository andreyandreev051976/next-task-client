import { defineStyle, forwardRef, PopoverContent, PopoverContentProps } from '@chakra-ui/react';

const popoverContent = defineStyle({
  mt: '0.5rem',
  w: 'full',
});

export const PopoverContainer = forwardRef<PopoverContentProps, 'div'>(({ children, ...props }, ref) => {
  return (
    <PopoverContent {...props} {...popoverContent} ref={ref}>
      {children}
    </PopoverContent>
  );
});
