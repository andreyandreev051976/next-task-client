import { BoxProps } from "@chakra-ui/react";
import { PropsWithChildren } from "react";

export interface IFrame extends PropsWithChildren<BoxProps> {}
