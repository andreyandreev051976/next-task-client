import { Box, chakra } from "@chakra-ui/react";
import { IFrame } from "./frame.type";
import { Title } from "./components/title";

const Wrapper = chakra(Box, {
  baseStyle: {
    padding: 8,
    borderRadius: 20,
    boxShadow: '0px 6px 8px 0px rgba(34, 60, 80, 0.2)',
    background: 'white'
  }
})

export const Frame = ({ children, ...props }: IFrame) => {
  return <Wrapper {...props}>{children}</Wrapper>;
};

Frame.Title = Title;
