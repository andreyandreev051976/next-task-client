import { ComponentPropsWithRef } from 'react';
import { Button } from '@chakra-ui/react';

export type IButtonSelectItem = ComponentPropsWithRef<typeof Button>;
