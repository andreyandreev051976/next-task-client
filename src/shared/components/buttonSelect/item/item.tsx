import { Button } from '@chakra-ui/react';

import { IButtonSelectItem } from './item.type';
import { WithSelect, withSelect } from '../../../logic/select';

export const ButtonSelectItem = withSelect<WithSelect<IButtonSelectItem>>(({ isSelected, isActive, ...props }) => {
  return <Button {...props} isActive={isSelected || isActive} />;
});
