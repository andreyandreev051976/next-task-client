import { ComponentPropsWithRef } from 'react';
import { HStack } from '@chakra-ui/react';
import { ISelect } from '../../logic/select';

export type IButtonSelect = ComponentPropsWithRef<typeof HStack> & ISelect;
