import { HStack } from '@chakra-ui/react';

import { IButtonSelect } from './buttonSelect.type';
import { Select } from '../../logic/select';

export const ButtonSelect = ({
  selected,
  onItemSelect,
  selectType = 'single',
  minToSelect,
  ...props
}: IButtonSelect) => {
  return (
    <Select selected={selected} onItemSelect={onItemSelect} selectType={selectType} minToSelect={minToSelect}>
      <HStack {...props} />
    </Select>
  );
};
