import { useEffect, useState } from "react";
import { INamespaceLoader } from "./namespaceLoader.type";
import i18next from "i18next";

export const NamespaceLoader = ({
  children,
  fallback = null,
  ns,
}: INamespaceLoader) => {
  const [isLoading, setIsLoading] = useState(() => !i18next.hasLoadedNamespace(ns));

  useEffect(() => {
    i18next.loadNamespaces(ns, () => {
      setIsLoading(false);
    });
  }, [ns]);

  if (isLoading) {
    return fallback;
  }

  return children;
};
