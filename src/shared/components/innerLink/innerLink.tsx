/* eslint-disable @typescript-eslint/no-explicit-any */
import { IInnerLink } from "./innerLink.type";
import { Link, forwardRef } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";
import { ROUTES } from "../../routes/routes.config";
import { ObjValues } from "../../types/alias";
import { useMemo } from "react";
import { getUrl } from "../../utils/url";

export const InnerLink = forwardRef(
  <To extends ObjValues<typeof ROUTES>>(
    { to, params, ...props }: IInnerLink<To>,
    ref: any
  ) => {
    const linkTo = useMemo(() => getUrl(to, params), [to, params]);
    return <Link {...props} to={linkTo} as={RouterLink} ref={ref} />;
  }
);
