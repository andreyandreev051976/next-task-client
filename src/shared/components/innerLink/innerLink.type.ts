import { LinkProps } from "@chakra-ui/react";
import { ObjValues } from "../../types/alias";
import { ROUTES } from "../../routes/routes.config";
import { GetParams } from "../../types/params";

export interface IInnerLink<To extends ObjValues<typeof ROUTES>>
  extends Omit<LinkProps, "as"> {
  to: To;
  params?: GetParams<To>;
}
