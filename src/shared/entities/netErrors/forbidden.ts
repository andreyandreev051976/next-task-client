import { NetworkError } from "./abstract-error";

export class Forbidden extends NetworkError {
    status: number = 403;
}