import { AxiosError } from "axios";

export class NetworkError extends AxiosError {
    static status: number;
}
