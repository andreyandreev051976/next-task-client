import { HStack } from "@chakra-ui/react";
import { IHeader } from "./header.type";
import { GRID_AREAS } from "../layout.contants";

export const Header = (props: IHeader) => {
  return (
    <HStack
      bg="var(--chakra-colors-gradients-coolBlues)"
      {...props}
      px="1rem"
      py="0.8rem"
      gridArea={ GRID_AREAS.HEADER }
      as="header"
    />
  );
};
