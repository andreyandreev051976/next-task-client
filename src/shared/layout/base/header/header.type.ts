import { BoxProps } from "@chakra-ui/react";

export interface IHeader extends Omit<BoxProps, 'as'> {}
