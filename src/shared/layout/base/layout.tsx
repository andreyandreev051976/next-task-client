import { Box, chakra } from "@chakra-ui/react";
import { Outlet } from "react-router-dom";
import { IContainer, ILayout } from "./layout.type";
import { GRID_AREAS } from "./layout.contants";
import { Header } from "./header";
import { useSidebarValue } from "./sidebar/sidebar.hooks";
import { Sidebar } from "./sidebar/sidebar";
import { motion } from "framer-motion";

export const BaseContainer = (props: IContainer) => {
  return <Box w="full" h="full" p="0.4rem" pl="1.2rem" {...props} gridArea={GRID_AREAS.CONTENT} />;
};

const LayoutContainer = chakra(motion.div, {
  baseStyle: {
    display: "grid",
    gridTemplateAreas: `"${GRID_AREAS.HEADER} ${GRID_AREAS.HEADER}" "${GRID_AREAS.SIDEBAR} ${GRID_AREAS.CONTENT}"`,
    gridTemplateRows: "3.2rem auto",
    transition: "0.25s ease-in",
  },
});

export const BaseLayout = (props: ILayout) => {
  const { isOpen } = useSidebarValue();
  return (
    <LayoutContainer
      h="full"
      {...props}
      gridTemplateColumns={`${isOpen ? 250 : 60}px auto`}
    />
  );
};

BaseLayout.Container = BaseContainer;
BaseLayout.Header = Header;
BaseLayout.Content = Outlet;
BaseLayout.Sidebar = Sidebar;
