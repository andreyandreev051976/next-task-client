export const GRID_AREAS = {
    SIDEBAR: 'side',
    HEADER: 'header',
    CONTENT: 'content'
}