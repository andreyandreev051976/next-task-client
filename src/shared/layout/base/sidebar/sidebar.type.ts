import { ButtonProps } from "@chakra-ui/react";
import {
  ComponentType,
  Dispatch,
  PropsWithChildren,
  ReactNode,
  SetStateAction,
} from "react";

export interface ISidebarCtxValue {
  isOpen: boolean;
  size: ButtonProps["size"];
}

export interface ISidebarCtxActions {
  setIsOpen: Dispatch<SetStateAction<ISidebarCtxValue["isOpen"]>>;
  close(): void;
  open(): void;
  toggle(): void;
}

export interface ISidebar extends PropsWithChildren {
  size?: ButtonProps["size"];
}

export interface IWithSidebar {
  <P>(Component: ComponentType<P>): ComponentType<P>;
}

export interface IStateConsumer {
  children(props: ISidebarCtxActions & ISidebarCtxValue): ReactNode;
}
