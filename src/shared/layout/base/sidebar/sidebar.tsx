import { useCallback, useMemo, useState } from "react";
import { ISidebar, IStateConsumer } from "./sidebar.type";
import { SidebarActionsCtx, SidebarValueCtx } from "./sidebar.context";
import { Panel } from "./components/panel";
import { useSidebarActions, useSidebarValue } from "./sidebar.hooks";
import { Burger } from "./components/burger";
import { Item } from "./components/item";
import { Group } from "./components/group";

export const Sidebar = ({ children, size = "md" }: ISidebar) => {
  const [isOpen, setIsOpen] = useState(false);

  const value = useMemo(() => ({ isOpen, size }), [isOpen, size]);

  const open = useCallback(() => setIsOpen(true), []);
  const close = useCallback(() => setIsOpen(false), []);
  const toggle = useCallback(() => setIsOpen((prev) => !prev), []);

  const actions = useMemo(
    () => ({ setIsOpen, open, close, toggle }),
    [close, open, toggle]
  );

  return (
    <SidebarActionsCtx.Provider value={actions}>
      <SidebarValueCtx.Provider value={value}>
        {children}
      </SidebarValueCtx.Provider>
    </SidebarActionsCtx.Provider>
  );
};

Sidebar.Panel = Panel;
Sidebar.Burger = Burger;
Sidebar.Item = Item;
Sidebar.Group = Group;
Sidebar.ActionConsumer = SidebarActionsCtx.Consumer;
Sidebar.ValueConsumer = SidebarValueCtx.Consumer;

const StateConsumer = ({ children }: IStateConsumer) => {
  const value = useSidebarValue();
  const actions = useSidebarActions();

  return children({ ...value, ...actions });
};
Sidebar.StateConsumer = StateConsumer;
