import { createContext } from "react";
import { ISidebarCtxActions, ISidebarCtxValue } from "./sidebar.type";

export const SidebarValueCtx = createContext({} as ISidebarCtxValue);
export const SidebarActionsCtx = createContext({} as ISidebarCtxActions);