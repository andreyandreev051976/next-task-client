import { IWithSidebar } from "./sidebar.type";
import { Sidebar } from "./sidebar";

export const withSidebar: IWithSidebar = (Component) => {
  const WrappedComponent = (props) => {
    return (
      <Sidebar>
        <Component {...props} />
      </Sidebar>
    );
  };

  WrappedComponent.displayName = `WithSidebar(${Component.displayName})`;

  return WrappedComponent;
};
