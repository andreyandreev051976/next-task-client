import { IconButton } from "@chakra-ui/react";
import { Sidebar } from "../../sidebar";
import { ChevronRightIcon } from "@chakra-ui/icons";
import { extendHandler } from "../../../../../utils/handler";
import { MouseEventHandler } from "react";
import { IBurger } from "./burger.type";

export const Burger = ({ onClick, ...props }: IBurger) => {
  return (
    <Sidebar.StateConsumer>
      {({ isOpen, size, toggle }) => (
        <IconButton
          isRound
          size={size}
          icon={
            <ChevronRightIcon transform={isOpen ? "rotate(180deg)" : "none"} />
          }
          {...props}
          aria-label="menu"
          onClick={extendHandler<MouseEventHandler<HTMLButtonElement>>(
            toggle,
            onClick
          )}
        />
      )}
    </Sidebar.StateConsumer>
  );
};
