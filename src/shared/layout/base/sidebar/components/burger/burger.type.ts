import { IconButtonProps } from "@chakra-ui/react";

export interface IBurger extends Omit<IconButtonProps, "aria-label"> {}
