import { Button, IconButton, Tooltip } from "@chakra-ui/react";
import { useSidebarValue } from "../../sidebar.hooks";
import { IItem } from "./item.type";

export const Item = ({ icon, children, ...props }: IItem) => {
  const { isOpen, size } = useSidebarValue();

  if (!isOpen) {
    return (
      <Tooltip label={ children } placement="left">
        <IconButton aria-label="menu-item" size={size} {...props} icon={icon} />
      </Tooltip>
    );
  }

  return (
    <Button
      w="full"
      justifyContent="flex-start"
      size={size}
      fontWeight={500}
      {...props}
      leftIcon={icon}
    >
      {children}
    </Button>
  );
};
