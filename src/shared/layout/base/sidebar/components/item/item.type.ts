import { ButtonProps } from "@chakra-ui/react";
import { ReactElement } from "react";

export interface IItem extends ButtonProps {
  icon: ReactElement;
}
