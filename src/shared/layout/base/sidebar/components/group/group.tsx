import { IconButton, Text, Tooltip, HStack, chakra } from "@chakra-ui/react";
import { IGroup } from "./group.type";
import { motion } from "framer-motion";
import { useSidebarValue } from "../../sidebar.hooks";

const LAST_STYLE = {
  pb: "0.8rem",
  mb: "0.4rem",
  borderBottom: "1px solid",
  borderBottomColor: "gray.400",
};

const GroupContainer = chakra(motion.div, {
  baseStyle: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    w: "full",
    _notLast: LAST_STYLE,
    _last: {
      _first: LAST_STYLE,
    },
  },
});

export const Group = ({ name, icon, children, ...props }: IGroup) => {
  const { isOpen, size } = useSidebarValue();
  let head = (
    <Text mb="0.4rem" noOfLines={isOpen ? undefined : 1} as={HStack} fontWeight="600">
      {icon} <Text>{name}</Text>
    </Text>
  );
  if (!isOpen) {
    head = (
      <Tooltip label={name} placement="left">
        <IconButton aria-label="group-name" icon={icon} size={size} />
      </Tooltip>
    );
  }

  return (
    <GroupContainer {...props}>
      {head}
      {children}
    </GroupContainer>
  );
};
