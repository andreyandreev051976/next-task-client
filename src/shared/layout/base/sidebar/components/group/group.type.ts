import { BoxProps } from "@chakra-ui/react";
import { ReactElement, ReactNode } from "react";

export interface IGroup extends BoxProps {
  name: ReactNode;
  icon: ReactElement;
}
