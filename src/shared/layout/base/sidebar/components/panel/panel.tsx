import { Box, chakra } from "@chakra-ui/react";
import { IPanel } from "./panel.type";
import { GRID_AREAS } from "../../../layout.contants";

const PanelContainer = chakra(Box, {
  baseStyle: {
    bg: "gray.100",
    position: 'relative',
    p: '0.8rem',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    gap: '0.5rem'
  },
});

export const Panel = (props: IPanel) => {
  return <PanelContainer {...props} gridArea={GRID_AREAS.SIDEBAR} h="full" />;
};
