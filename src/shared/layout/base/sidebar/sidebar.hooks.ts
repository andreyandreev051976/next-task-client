import { useContext } from "react";
import { SidebarActionsCtx, SidebarValueCtx } from "./sidebar.context";

export const useSidebarValue = () => useContext(SidebarValueCtx);
export const useSidebarActions = () => useContext(SidebarActionsCtx);
