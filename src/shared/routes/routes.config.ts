export const ROUTES = {
  BASE: "/",
  SPACES: "/spaces",
  SPACE_BY_ID: "/spaces/:id"
} as const;
