/* eslint-disable @typescript-eslint/no-explicit-any */
import { PropsWithChildren, useMemo } from 'react';
import { StoreApi, UseBoundStore } from 'zustand';

import { IModalElement, ModalProps, ModalState } from './modalManage.type';
import { createModalContext } from './modalManager.context';
import { createInitialState, createModalManagerController, IModalManagerState } from './modalManager.creator';
import { createUseModalManager } from './modalManaget.hook';

type CreateModalProviderProps<M> = {
  instanceInitialState: M;
  instanceController: UseBoundStore<StoreApi<IModalManagerState<M>>>;
  InstanceModalContext: React.Context<M>;
};

export const createModalManagerProvider = <M extends object>({
  instanceInitialState,
  instanceController,
  InstanceModalContext,
}: CreateModalProviderProps<M>) => {
  return ({ children }: PropsWithChildren<unknown>) => {
    const state = instanceController();

    const openFactory = useMemo(
      () => (name: keyof M) => (props: ModalProps<M>) => state.actions.open({ name, props }),
      [state.actions],
    );

    const closeFactory = useMemo(() => (name: keyof M) => () => state.actions.close({ name }), [state.actions]);

    const value = useMemo(() => {
      const modalKeys = Object.keys(instanceInitialState) as (keyof M)[];
      const result = modalKeys.reduce((acc, cur) => {
        const open = openFactory(cur);
        const close = closeFactory(cur);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        acc[cur] = { ...state.state[cur], open, close };
        return acc;
      }, state.state);
      return result as unknown as ModalState<M>;
    }, [state.state, openFactory, closeFactory]);

    return (
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      <InstanceModalContext.Provider value={value}>
        {children}
        {Object.values<IModalElement<any, M>>(value).map((Modal, key) => (
          <Modal.Component key={key} {...Modal.props} />
        ))}
      </InstanceModalContext.Provider>
    );
  };
};

export const createModalManager = <M extends object>(instances: M) => {
  const initialState = createInitialState(instances);
  const ctx = createModalContext(initialState);
  const controller = createModalManagerController(initialState);
  const Provider = createModalManagerProvider({
    instanceInitialState: initialState,
    instanceController: controller,
    InstanceModalContext: ctx,
  });
  const { Consumer } = ctx;
  const useModalManager = createUseModalManager(ctx);
  return {
    ctx,
    controller,
    Provider,
    Consumer,
    useModalManager,
  };
};
