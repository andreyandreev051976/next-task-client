export type IModalElement<K extends keyof M, M> = {
  Component: ModalComponents<M>[K];
  props: ModalProps<M>[K];
  open: (props?: ModalProps<M>[K]) => void;
  close: () => void;
  isOpen: boolean;
};

export type ModalState<M> = {
  [K in keyof M]: IModalElement<K, M>;
};

export type ModalComponents<M> = {
  [k in keyof M]: M[k];
};
export type ModalProps<M extends ModalSetup<M>> = {
  [k in keyof M]: React.ComponentProps<M[k]>;
};

export type ModalActions<M extends ModalSetup<M>> = {
  open(value: OpenModalType<M>): void;
  close(value: CloseModalType<M>): void;
};

type OpenModalType<M extends ModalState<M>> = {
  name: keyof M;
  props: ModalProps<M>;
};

type CloseModalType<M> = {
  name: keyof M;
};

type ModalSetup<S extends ModalSetup<S>> = {
  [k in keyof S]: ModalState<S>[k]['Component'];
};
