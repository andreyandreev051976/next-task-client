import { create } from 'zustand';

import { ModalActions, ModalState } from './modalManage.type';
import { noop } from '../../utils/noop';

export function createInitialState<S extends object>(modalSetup: S): ModalState<S> {
  const modalMethods = { isOpen: false, open: noop, close: noop };
  const modalNames = Object.keys(modalSetup) as (keyof S)[];
  const result = modalNames.reduce((acc, modalName) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    acc[modalName] = {
      ...modalMethods,
      Component: modalSetup[modalName],
    };
    return acc;
  }, {} as ModalState<S>);
  return result;
}

export interface IModalManagerState<S> {
  state: S;
  actions: ModalActions<S>;
}

export const createModalManagerController = <S>(initialState: S) =>
  create<IModalManagerState<S>>((set) => ({
    state: initialState,
    actions: {
      open({ name, props }) {
        set((state) => ({
          ...state,
          state: {
            ...state.state,
            [name]: {
              ...state.state[name],
              isOpen: true,
              props,
            },
          },
        }));
      },
      close({ name }) {
        set((state) => ({
          ...state,
          state: {
            ...state.state,
            [name]: {
              ...state.state[name],
              isOpen: false,
              props: {},
            },
          },
        }));
      },
    },
  }));
