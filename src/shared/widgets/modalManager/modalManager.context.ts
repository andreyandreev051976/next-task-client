import { createContext } from 'react';

export function createModalContext<M extends object>(initialState?: M) {
  return createContext<M>((initialState ?? {}) as M);
}

export const ModalContext = createModalContext<object>();
