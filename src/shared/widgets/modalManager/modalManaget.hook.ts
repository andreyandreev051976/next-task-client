import { useContext } from 'react';

import { ModalState } from './modalManage.type';

export function createUseModalManager<C extends ModalState<T>, T>(context: React.Context<C>) {
  return () => {
    const c = useContext<C>(context);
    if (!c) throw new Error('useModalManager hook cannot be used outside of the modal provider');
    return c;
  };
}
