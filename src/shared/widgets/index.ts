import { CreateSpace } from "../../features/space/ui/create";
import { createModalManager } from "./modalManager";

export * from "./modalManager";

export const GlobalModalManager = createModalManager({
  createSpace: CreateSpace,
});
