export type Split<S extends string, D extends string> = string extends S
  ? string[]
  : S extends ""
  ? []
  : S extends `${infer T}${D}${infer U}`
  ? [T, ...Split<U, D>]
  : [S];

export type HasParam<T> = T extends `:${infer P}` ? P : never;
export type PickByParam<T extends unknown[]> = T extends [infer F, ...infer L]
  ? [HasParam<F>, ...PickByParam<L>]
  : [HasParam<T>];

export type GetParams<
  S extends string,
  K extends string = PickByParam<Split<S, "/">>[number]
> = {
  [Key in K]: string | number;
};
