import { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import { IResourcePlugin } from "./plugins/create/create.type";
import { GetParams } from "../../types/params";

interface IResourceUrl<Url extends string> {
  url: Url;
  urlParams?: GetParams<Url>;
}

type IResourceConfig<D, R, E, C, Url extends string> = Omit<
  AxiosRequestConfig<D>,
  "url"
> &
  IResourceUrl<Url> & {
    onSuccess?: <T = AxiosResponse<R, C>>(data: T) => T;
    onError?: <T = AxiosError<E, C>>(error: T) => T;
    onFinally?: () => void;
    plugins?: IResourcePlugin<D, D, E>[];
  };

export interface IResourceCaller<D= unknown, R = unknown, C = object, Url extends string = string> {
  (
    params?: Omit<AxiosRequestConfig<D>, "url"> & Omit<IResourceUrl<Url>, "url">
  ): Promise<AxiosResponse<R, C>>;
}

export interface IResource {
  <
    D = unknown,
    R = unknown,
    E = unknown,
    C extends Omit<AxiosRequestConfig<unknown>, "url"> = object
  >(): <Url extends string = string>(
    params: IResourceConfig<D, R, E, C, Url>
  ) => IResourceCaller<D, R, C, Url>;
}
