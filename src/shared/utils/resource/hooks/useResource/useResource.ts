import { useCallback, useState } from "react";
import { IPendingResponse, IState, IUseResource } from "./useResource.type";

const PENDING_STATE: IPendingResponse = {
  isLoading: false,
  isSuccess: null,
  errors: [],
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const useResource: IUseResource = (resource) => {
  const [state, setState] = useState<IState>({ ...PENDING_STATE });

  const request: ReturnType<IUseResource>['request'] = useCallback(
    (...params) => {
      setState({ ...PENDING_STATE });
      return resource(...params)
        .then((d) => {
          setState({ isLoading: false, isSuccess: true, errors: [] });
          return d;
        })
        .catch((e) => {
          setState({ isLoading: false, isSuccess: false, errors: e });
          throw e;
        });
    },
    [resource]
  );

  return {
    state,
    request,
  };
};
