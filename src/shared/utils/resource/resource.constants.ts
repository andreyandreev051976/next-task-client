export const isSuccess = (status: number) => status >= 200 && status < 300;
export const isRedirect = (status: number) => status >= 300 && status < 400;