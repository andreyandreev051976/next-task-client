import { ERROR_INSTANCE_BY_STATUS } from "../../../../entities/netErrors";
import { createErrorPlugin } from "../create";
import { IAutoError } from "./autoError.type"

export const autoError: IAutoError = ({ onError = {} } = {}) => {
    return createErrorPlugin((error) => {
        const { status } = error;
        if(!status){
            throw error
        }
        const ErrorInstance = ERROR_INSTANCE_BY_STATUS[status];

        if(!ErrorInstance){
            throw error
        }

        const errorHandler = onError[status];
        errorHandler?.(error);

        throw new ErrorInstance(
            error.message,
            error.code,
            error.config,
            error.request,
            error.response
        )
    })
}