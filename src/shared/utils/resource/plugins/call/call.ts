import { ICallPlugin } from "./call.type";

export const callPlugin: ICallPlugin = (fns = []) => (...args) =>  fns.forEach(fn => fn(...args));