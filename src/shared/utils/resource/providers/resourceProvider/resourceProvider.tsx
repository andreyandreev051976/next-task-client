import { createContext, useContext } from "react";
import { ICreateResourceProvider } from "./resourecProvider.type";
import { useResource as useResourceCore } from "../../hooks";

export const createResourceProvider: ICreateResourceProvider = (resource) => {
  const Context = createContext({}) as unknown as ReturnType<ICreateResourceProvider>['Context'];
  const Provider: ReturnType<ICreateResourceProvider>["Provider"] = ({
    children,
  }) => {
    const value = useResourceCore(resource);

    return <Context.Provider value={value}>{children}</Context.Provider>;
  };
  const Consumer = Context.Consumer;
  const useResource = () => useContext(Context);

  return {
    Context,
    Provider,
    Consumer,
    useResource
  };
};
