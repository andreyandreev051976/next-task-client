export interface INoopValue {
    <V>(value: V): V;
}