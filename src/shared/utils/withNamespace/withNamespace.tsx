import { NamespaceLoader } from "../../components/i18n/namespaceLoader";
import { toArray } from "../array";
import { IWithNamespace } from "./withNamespace.type";

export const withNamespace: IWithNamespace = (namespaces) => {
  const computedNamespace = toArray(namespaces);

  return (Component) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const WrappedComponent = (props: any) => {
      return (
        <NamespaceLoader ns={computedNamespace}>
          <Component {...props} />
        </NamespaceLoader>
      );
    };

    WrappedComponent.displayName = `WithNamespace(${Component.displayName})`;

    return WrappedComponent;
  };
};
