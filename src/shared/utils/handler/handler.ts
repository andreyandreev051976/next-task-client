import { IExtendHandler } from "./handler.type";

export const extendHandler: IExtendHandler =
  (base, ...acts) =>
  (...args) => {
    acts.forEach((act) => act?.(...args));
    return base?.(...args);
  };
