import { AnyFn } from "../../types/alias";

export interface IExtendHandler {
  <H extends AnyFn>(base: (H | undefined), ...acts: Array<H | undefined>): (...params: Parameters<H>) => ReturnType<H>;
}
