export type ToArray<T> = T extends unknown[] ? T : [T];

export interface IToArray {
    <T>(target: T): ToArray<T>
}