import { GetParams } from "../../types/params";

const REPLACE_PATTERN = /:([^/]*)/g;

export const getUrl = <U extends string>(url: U, params?: GetParams<U>) => {
  const computedParams = (params || {}) as Record<string, string>;
  return url.replace(
    REPLACE_PATTERN,
    (key) => computedParams[key.slice(1)] || ""
  );
};
