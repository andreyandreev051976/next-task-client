import { extendTheme } from '@chakra-ui/react'
import "@fontsource/open-sans";
import { COOL_BLUES, ROYAL_BLUE } from './tokens';

export const theme = extendTheme({
  fonts: {
    body: `'Open Sans', sans-serif`,
  },
  colors: {
    gradients: {
      royalBlue: ROYAL_BLUE,
      coolBlues: COOL_BLUES
    }
  }
})