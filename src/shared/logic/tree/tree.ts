import { Node } from "./node";
import { INode } from "./node/node.type";
import { ITree } from "./tree.type";

export class Tree<V> implements ITree<V> {
  root: INode<V> | null;

  constructor() {
    this.root = null;
  }

  add(child: V, parent?: INode<V>): INode<V> {
    const node = new Node(child);

    if (!this.root) {
      this.root = node;
      return node;
    }
    if (!parent) {
      return node;
    }
    node.parent = parent;
    parent.children.push(node);
    return node;
  }

  remove(node: INode<V>): INode<V> {
    if (this.root === node) {
      this.root = null;
    }
    if (!node.parent) {
      return node;
    }
    node.parent.children = node.parent.children.filter(
      (tNode) => tNode !== node
    );
    return node;
  }

  find(node: INode<V>): INode<V> | null {
    if (!this.root) {
      return null;
    }
    const stack = [this.root];
    while (stack.length !== 0) {
      const target = stack.pop();
      if (!target) {
        return null;
      }
      if (target === node) {
        return target;
      }
      stack.push(...target.children);
    }

    return null;
  }
}
