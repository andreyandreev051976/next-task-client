import { INode } from "./node.type";

export class Node<V> implements INode<V> {
  value: V;
  children: INode<V>[];
  parent: INode<V> | null;

  constructor(value: V) {
    this.value = value;
    this.parent = null;
    this.children = [];
  }
}
