export interface INode<D> {
  value: D;
  parent: INode<D> | null;
  children: Array<INode<D>>;
}
