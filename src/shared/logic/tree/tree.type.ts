import { INode } from "./node/node.type";

export interface ITree<V> {
  root: INode<V> | null;
  find(node: INode<V>): INode<V> | null;
  add(child: V, parent: INode<V>): INode<V>;
  remove(node: INode<V>): INode<V>;
}
