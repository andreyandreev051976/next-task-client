import { createContext } from 'react';

import { ISelectItem } from './item/item.type';

export type ISelectCtx = {
  selected: ISelectItem[];
  onItemSelect: (value: ISelectItem) => void;
};

export const SelectCtx = createContext(null as unknown as ISelectCtx);
