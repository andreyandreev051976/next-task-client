import { PropsWithChildren, useMemo, useState } from 'react';

import { ISelectItem } from './item/item.type';
import { ISelectCtx, SelectCtx } from './select.context';
import { ISelect } from './select.type';

const resolveSelected = (
  fn: Exclude<ISelect['onItemSelect'], undefined>,
  selected: ISelectCtx['selected'],
  type: ISelect['selectType'],
  minToSelect: Exclude<ISelect['minToSelect'], undefined>,
) => {
  return ((value) => {
    const isSelected = selected.includes(value);
    if (type === 'single') {
      const nextValue = isSelected ? [] : [value];
      fn(nextValue.length < minToSelect ? selected : nextValue);
      return;
    }
    const nextValue = isSelected ? selected.filter((item) => item !== value) : [...selected, value];
    fn(nextValue.length < minToSelect ? selected : nextValue);
  }) as ISelectCtx['onItemSelect'];
};

export const Select = ({
  children,
  selectType,
  selected,
  minToSelect = 0,
  onItemSelect,
}: PropsWithChildren<ISelect>) => {
  const [_selected, _setSelected] = useState<ISelectItem[]>([]);
  const controls = useMemo(() => {
    if (selected !== undefined && onItemSelect !== undefined) {
      return {
        selected,
        onItemSelect: resolveSelected(onItemSelect, selected, selectType, minToSelect),
      };
    }
    return {
      selected: _selected,
      onItemSelect: resolveSelected(_setSelected, _selected, selectType, minToSelect),
    } as unknown as ISelectCtx;
  }, [_selected, selected, onItemSelect, selectType, minToSelect]);

  return <SelectCtx.Provider value={controls}>{children}</SelectCtx.Provider>;
};
