import { useContext } from 'react';

import { SelectCtx } from './select.context';

export const useSelectPrivate = () => useContext(SelectCtx);
