import {
  ComponentType,
  MouseEventHandler,
  ReactNode,
  useMemo,
} from "react";

import { ISelectItem } from "./item/item.type";
import { useSelectPrivate } from "./select.hooks";
import { AnyFn } from "../../types/alias";

interface ISelectElementData {
  isSelected?: boolean;
  selectValue: ISelectItem;
}

export interface ISelectElement {
  selectValue: ISelectItem;
  isSelected?: boolean;
  children?: ReactNode | ((ctx: ISelectElementData) => ReactNode);
  onClick?: MouseEventHandler;
}

const isFn = (target: unknown): target is AnyFn => typeof target === "function";

export type WithSelect<C extends object> = Omit<C, keyof ISelectElement> & ISelectElement;

export const withSelect = <
  P extends object,
  C extends ComponentType<WithSelect<P>> = ComponentType<WithSelect<P>>
>(
  Component: C
) => {
  const WrappedComponent = ({
    selectValue,
    onClick,
    children,
    ...props
  }: WithSelect<P>) => {
    const { selected, onItemSelect } = useSelectPrivate();
    const isSelected = useMemo(
      () => selected.includes(selectValue),
      [selected, selectValue]
    );
    const handleClick: MouseEventHandler = (event) => {
      onItemSelect(selectValue);
      onClick?.(event);
    };

    const content = isFn(children)
      ? children({ isSelected, selectValue })
      : children;

    return (
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      <Component {...props} isSelected={isSelected} onClick={handleClick}>
        {content}
      </Component>
    );
  };

  WrappedComponent.displayName = `WithSelect(${Component.displayName})`;

  return WrappedComponent;
};
