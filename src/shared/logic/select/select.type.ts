import { ISelectItem } from './item/item.type';

type ISelectType = 'single' | 'multiple';

export type ISelect = {
  selected?: ISelectItem[];
  onItemSelect?: (value: Exclude<ISelect['selected'], undefined>) => void;
  selectType?: ISelectType;
  minToSelect?: number;
};
