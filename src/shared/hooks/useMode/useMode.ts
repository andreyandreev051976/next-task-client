import { ColorMode, SystemProps, useColorMode } from '@chakra-ui/react';

export type UseModeParameters<P = undefined> = Record<ColorMode, SystemProps | ((params: P) => SystemProps)>;

export const useMode = <P = undefined>(variants: UseModeParameters<P>, params?: P) => {
  const { colorMode } = useColorMode();

  const currentStyles = variants[colorMode] || variants.light;
  const getter = typeof currentStyles === 'function' ? currentStyles : () => currentStyles;
  const computedStyles = getter(params as unknown as P);
  return computedStyles;
};
