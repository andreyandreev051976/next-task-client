import { useCallback, useRef } from 'react';
import { SystemProps } from '@chakra-ui/react';

export type UseBoolStylesParameters = [SystemProps, SystemProps];

export const useBoolStyles = (variants: UseBoolStylesParameters) => {
  const varRef = useRef(variants);
  varRef.current = variants;
  return useCallback((condition: boolean) => varRef.current[+condition], []);
};
