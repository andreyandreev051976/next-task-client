import { ReactNode } from 'react';
import { useMediaQuery, useTheme } from '@chakra-ui/react';
import { toArray } from '../../utils/array';

type BreakPoints = 'base' | 'sm' | 'md' | 'lg' | 'xl' | '2xl';

type IMedia = BreakPoints | BreakPoints[];

const DEFAULT_KEYS: IMedia = ['base', 'sm', 'md', 'lg', 'xl', '2xl'];

interface IUseMediaHook {
  <K extends IMedia>(keys?: K): K extends unknown[] ? boolean[] : [boolean];
}

export const useMedia: IUseMediaHook = <K extends IMedia>(keys: K = DEFAULT_KEYS as K) => {
  const toPick = toArray(keys);
  const { __breakpoints } = useTheme();
  const breakPoints = __breakpoints?.asObject || {};
  const points = toPick.reduce((p, key) => {
    const target = breakPoints[key];
    if (target !== undefined) {
      p.push(target);
    }
    return p;
  }, [] as string[]);
  const queries = points.map((p) => `(min-width: ${p})`);
  return useMediaQuery(queries) as unknown as K extends unknown[] ? boolean[] : [boolean];
};

interface IUseMedia<K extends IMedia> {
  children({ media }: { media: ReturnType<IUseMediaHook> }): ReactNode;
  media?: K;
}

export const UseMedia = <K extends IMedia>({ children, media }: IUseMedia<K>) => {
  const computedMedia = useMedia(media);
  return children({ media: computedMedia });
};
