import { useMemo, useState } from 'react';

interface IControls<T extends string | number> {
  value?: T;
  setValue?: (value: T) => void;
  initialValue?: T;
}

export const useControls = <T extends string | number>(controls: IControls<T>) => {
  const [innerValue, setInnerValue] = useState(controls.initialValue || '');

  return useMemo(() => {
    if (typeof controls.value !== 'undefined' && typeof controls.setValue === 'function') {
      return {
        value: controls.value,
        setValue: controls.setValue,
      };
    }
    return {
      value: innerValue,
      setValue: setInnerValue,
    };
  }, [innerValue, controls.setValue, controls.value]);
};
