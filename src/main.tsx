import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { ChakraProvider } from "@chakra-ui/react";
import { runSetup } from "./setup/core/setup.ts";
import { setupI18n } from "./setup/i18n/index.ts";
import { theme } from "./shared/styleSystem/theme.ts";

runSetup([setupI18n])

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ChakraProvider theme={ theme }>
      <App />
    </ChakraProvider>
  </React.StrictMode>
);
