import { useEffect } from "react";
import { mySpaceStore } from "../../../../entities/space/model";
import { SpaceList } from "../../../../features/space/ui";

export const MySpaces = () => {
  const { state, actions } = mySpaceStore();

  useEffect(() => {
    actions.getMy();
  }, [actions]);

  const { isLoading, spaces, errors } = state;
  if (isLoading) {
    return null;
  }
  if (errors.length) {
    return null;
  }
  return <SpaceList spaces={spaces} />;
};
