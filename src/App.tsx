import { RouterProvider, createBrowserRouter } from "react-router-dom";
import "./App.css";
import { BaseLayout } from "./shared/layout";
import { ROUTES } from "./shared/routes/routes.config";
import { useEffect } from "react";
import { UserAvatar } from "./entities/user/ui";
import { Box } from "@chakra-ui/react";
import { Sidebar } from "./shared/layout/base/sidebar/sidebar";
import { SpaceSidebar } from "./entities/space/ui";
import { SpacePage } from "./pages/space";
import { GlobalModalManager } from "./shared/widgets";
import { userStore } from "./entities/user/model";

const router = createBrowserRouter([
  {
    path: ROUTES.BASE,
    element: (
      <Sidebar size="sm">
        <BaseLayout>
          <BaseLayout.Header>
            <Box ml="auto">
              <UserAvatar />
            </Box>
          </BaseLayout.Header>
          <BaseLayout.Content />
          <Sidebar.Panel>
            <Sidebar.Burger
              position="absolute"
              right="0"
              transform="translateX(50%)"
              zIndex="1"
              boxShadow="md"
            />
            <SpaceSidebar />
          </Sidebar.Panel>
        </BaseLayout>
      </Sidebar>
    ),
    children: [
      {
        path: ROUTES.SPACES,
        element: <SpacePage />,
      },
      {
        path: "*",
        element: <>not found</>,
      },
    ],
  },
]);

function App() {
  useEffect(() => {
    userStore.getState().actions.getMeData();
  }, []);
  return (
    <GlobalModalManager.Provider>
      <RouterProvider router={router} />
    </GlobalModalManager.Provider>
  );
}

export default App;
