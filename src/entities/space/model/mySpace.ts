import { create } from "zustand";
import { ISpaceDto, createSpace } from "./space";
import { getMy } from "../api/getMy";

interface IMySpaceState {
  spaces: ISpaceDto[];
  isLoading: boolean;
  errors: unknown[];
}

interface IMySpaceActions {
  getMy(): Promise<ISpaceDto[]>;
}

interface IMySpaceStore {
  state: IMySpaceState;
  actions: IMySpaceActions;
}

export const mySpaceStore = create<IMySpaceStore>((set) => ({
  state: {
    spaces: [],
    isLoading: true,
    errors: [],
  },
  actions: {
    async getMy() {
      set((state) => ({
        ...state,
        state: { ...state.state, isLoading: true },
      }));
      try {
        const { data } = await getMy();
        const mappedData = data.map(createSpace);
        set((state) => ({
          ...state,
          state: { spaces: mappedData, isLoading: false, errors: [] },
        }));
        return mappedData;
      } catch (error) {
        set((state) => ({
          ...state,
          state: { spaces: [], isLoading: false, errors: [error] },
        }));
        return [];
      }
    },
  },
}));
