import { IUserDto, createUser } from "../../user/model";

export interface ISpaceDto {
  id: number;
  name: string;
  isPrivate: string;
  users: Array<IUserDto>;
}

export interface ICreateSpaceDto {
  name: string;
  isPrivate: boolean;
  users: Array<IUserDto['id']>;
}

export function createSpace(dto: ISpaceDto): ISpaceDto {
  return {
    id: dto.id,
    name: dto.name,
    isPrivate: dto.isPrivate,
    users: dto.users.map(createUser),
  };
}
