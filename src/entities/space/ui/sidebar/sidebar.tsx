import { Icon } from "@chakra-ui/react";
import { Sidebar } from "../../../../shared/layout/base/sidebar/sidebar";
import { MdDeveloperBoard } from "react-icons/md";
import { useTranslation } from "react-i18next";
import { AddIcon } from "@chakra-ui/icons";
import { MdGridView } from "react-icons/md";
import { InnerLink } from "../../../../shared/components/innerLink";
import { ROUTES } from "../../../../shared/routes/routes.config";
import { GlobalModalManager } from "../../../../shared/widgets";

export const SpaceSidebar = () => {
  const { t } = useTranslation();
  return (
    <Sidebar.Group
      icon={<Icon as={MdDeveloperBoard} />}
      name={t("shared:Space")}
    >
      <Sidebar.Item as={InnerLink} to={ROUTES.SPACES} icon={<MdGridView />}>
        {t("shared:All")}
      </Sidebar.Item>
      <GlobalModalManager.Consumer>
        {({ createSpace }) => (
          <Sidebar.Item
            onClick={() => {
              createSpace.open();
            }}
            icon={<AddIcon />}
          >
            {t("shared:Add")}
          </Sidebar.Item>
        )}
      </GlobalModalManager.Consumer>
    </Sidebar.Group>
  );
};
