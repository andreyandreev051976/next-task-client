import {
  Box,
  Card,
  CardBody,
  Heading,
  Icon,
  Tooltip,
  VStack,
} from "@chakra-ui/react";
import { ISpaceCard } from "./card.type";
import { MdLockOutline } from "react-icons/md";
import { MdLockOpen } from "react-icons/md";
import { InnerLink } from "../../../../shared/components/innerLink";
import { ROUTES } from "../../../../shared/routes/routes.config";

export const SpaceCard = ({ name, isPrivate, id }: ISpaceCard) => {
  return (
    <InnerLink to={ROUTES.SPACE_BY_ID} params={{ id }}>
      <Card colorScheme="orange" size="md">
        <CardBody>
          <VStack>
            <Heading size="md">{name}</Heading>
            <Box position="absolute" top=".2rem" right=".2rem">
              <Tooltip label={isPrivate ? "Private" : "Public"}>
                <Box h="1rem">
                  <Icon as={isPrivate ? MdLockOutline : MdLockOpen} />
                </Box>
              </Tooltip>
            </Box>
          </VStack>
        </CardBody>
      </Card>
    </InnerLink>
  );
};
