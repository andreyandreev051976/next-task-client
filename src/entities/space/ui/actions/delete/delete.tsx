import { Button, forwardRef } from "@chakra-ui/react";
import { IDeleteSpace } from "./delete.type";
import { extendHandler } from "../../../../../shared/utils/handler";
import { deleteSpace } from "../../../api/delete";

export const DeleteSpace = forwardRef(
  ({ space, onClick, ...props }: IDeleteSpace, ref) => {
    const handleClick = extendHandler(onClick, async () => {
      await deleteSpace({ urlParams: { id: space.id } });
    });
    return <Button {...props} onClick={handleClick} ref={ref} />;
  }
);
