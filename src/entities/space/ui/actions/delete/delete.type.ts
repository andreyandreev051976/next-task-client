import { ButtonProps } from "@chakra-ui/react";
import { ISpaceDto } from "../../../model/space";

export interface IDeleteSpace extends ButtonProps {
  space: ISpaceDto;
}
