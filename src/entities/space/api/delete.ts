import { resource } from "../../../shared/utils/resource";
import { ISpaceDto } from "../model/space";

const API_URL = import.meta.env.VITE_API_TASK_URL;

export const deleteSpace = resource<never, ISpaceDto>()({
  method: "DELETE",
  baseURL: API_URL,
  url: `/space/:id`,
  withCredentials: true,
});
