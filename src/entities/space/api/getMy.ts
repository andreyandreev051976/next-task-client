import { resource } from "../../../shared/utils/resource";
import { ISpaceDto } from "../model/space";

const API_URL = import.meta.env.VITE_API_TASK_URL;

export const getMy = resource<never, ISpaceDto[]>()({
  method: "GET",
  baseURL: API_URL,
  url: `/space/my`,
  withCredentials: true,
});

