import { resource } from "../../../shared/utils/resource";
import { ISpaceDto, ICreateSpaceDto } from "../model/space";

const API_URL = import.meta.env.VITE_API_TASK_URL;

export const createSpace = resource<ICreateSpaceDto, ISpaceDto>()({
  method: "POST",
  baseURL: API_URL,
  url: `/space`,
  withCredentials: true,
});
