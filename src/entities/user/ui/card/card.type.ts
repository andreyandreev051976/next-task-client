import { IUserDto } from "../../model";

export interface IUserCard extends IUserDto {
    isSelected?: boolean;
}
