import {
  Avatar,
  Box,
  Card,
  CardBody,
  Flex,
  Heading,
  Text,
} from "@chakra-ui/react";
import { IUserCard } from "./card.type";

export const UserCard = ({ name, email, isSelected }: IUserCard) => {
  return (
    <Card size="sm" variant={isSelected ? "filled" : "elevated"}>
      <CardBody>
        <Flex flex="1" gap="4" alignItems="center" flexWrap="wrap">
          <Avatar size="xs" name={name || email} />
          <Box>
            <Heading size="sm">{name || email}</Heading>
            {name && <Text>{email}</Text>}
          </Box>
        </Flex>
      </CardBody>
    </Card>
  );
};
