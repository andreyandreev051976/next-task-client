import { Avatar, SkeletonCircle } from "@chakra-ui/react";
import { userStore as useUserStore } from "../../model/user";
import { IAvatar } from "./avatar.type";

export const UserAvatar = (props: IAvatar) => {
  const isInitialized = useUserStore(state => state.state.initialized);

  if (!isInitialized) {
    return <SkeletonCircle w="2rem" h="2rem" />;
  }

  return <Avatar {...props} size="sm" />;
};
