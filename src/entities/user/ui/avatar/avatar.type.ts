import { AvatarProps } from "@chakra-ui/react";

export interface IAvatar extends AvatarProps {}