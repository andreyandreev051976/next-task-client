import { StoreApi, UseBoundStore, create } from "zustand";
import { IUserDto, IUserStore, createUser } from "./user";
import { getUsers } from "../api/getUsers";

interface IAllUsersState {
  users: IUserDto[];
  isLoading: boolean;
  errors: unknown[];
}

interface IAllUsersActions {
  getAll(): Promise<IUserDto[]>;
}

interface IAllUsersGetters {
  withoutCurrent(userStore: UseBoundStore<StoreApi<IUserStore>>): IUserDto[];
}

interface IAllUsers {
  state: IAllUsersState;
  actions: IAllUsersActions;
  getters: IAllUsersGetters;
}

export const allUsersStore = create<IAllUsers>((set, get) => ({
  state: {
    users: [],
    isLoading: true,
    errors: [],
  },
  actions: {
    async getAll() {
      set((state) => ({
        ...state,
        state: { ...state.state, isLoading: true },
      }));
      try {
        const { data } = await getUsers();
        const mappedData = data.map(createUser);
        set((state) => ({
          ...state,
          state: { users: mappedData, isLoading: false, errors: [] },
        }));
        return mappedData;
      } catch (error) {
        set((state) => ({
          ...state,
          state: { users: [], isLoading: false, errors: [error] },
        }));
        return [];
      }
    },
  },
  getters: {
    withoutCurrent(userStore) {
      const { state } = userStore.getState();
      const { initialized } = state;
      if (!initialized) {
        return get().state.users;
      }

      const { id } = state;

      return get().state.users.filter((user) => user.id !== id);
    },
  },
}));
