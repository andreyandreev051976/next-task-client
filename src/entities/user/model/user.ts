import { create } from "zustand";
import { IRole, RoleDto, createRole } from "../../role/model/role";
import { getMe } from "../api/getMe";
import { AxiosError } from "axios";

export interface IUserDto {
  id: number;
  email: string;
  name: string;
  roles: Array<RoleDto>;
}

export interface IUser extends IUserDto {
  roles: Array<IRole>;
}

export function createUser(user: IUser): IUser {
  return {
    id: user.id,
    name: user.name,
    email: user.email,
    roles: user.roles?.map(createRole),
  };
}

export interface IUserStore {
  state: { initialized: false } | ({ initialized: true } & IUser);
  actions: {
    getMeData(): void;
  };
}

export const userStore = create<IUserStore>((set) => ({
  state: {
    initialized: false,
  },
  actions: {
    async getMeData() {
      try {
        const response = await getMe();
        const user = createUser(response.data);
        set((state) => ({
          ...state,
          state: { initialized: true, ...user },
        }));
      } catch (error) {
        if (error instanceof AxiosError) {
          if (error.response?.status === 401) {
            const currentLoc = window.location.href;
            const url = new URL(
              `https://local-sso.my-awesome-domain.ru:5174/login`
            );
            url.searchParams.append("retpath", currentLoc);
            window.location.replace(url);
          }
        }
        set((state) => ({ ...state, state: { initialized: false } }));
      }
    },
  },
}));
