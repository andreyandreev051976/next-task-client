import { resource } from "../../../shared/utils/resource";
import { IUserDto } from "../model/user";

const API_URL = import.meta.env.VITE_API_AUTH_URL;

export const getUsers = resource<never, IUserDto[]>()({
  method: "GET",
  baseURL: API_URL,
  url: `/users/`,
  withCredentials: true,
});
