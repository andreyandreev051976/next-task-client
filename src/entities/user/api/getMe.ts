import { resource } from "../../../shared/utils/resource";
import { IUserDto } from "../model/user";

const API_URL = import.meta.env.VITE_API_AUTH_URL;

export const getMe = resource<never, IUserDto>()({
  method: "POST",
  baseURL: API_URL,
  url: `/auth/me`,
  withCredentials: true,
});
