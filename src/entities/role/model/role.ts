export interface RoleDto {
  id: number;
  value: string;
  description: string;
}

export interface IRole extends RoleDto {}

export function createRole(role: IRole): IRole {
  return {
    id: role.id,
    value: role.value,
    description: role.description,
  };
}
